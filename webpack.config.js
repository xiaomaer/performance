var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
module.exports = {
    entry: './index.js',
    output: {
        path: 'build',
        publicPath: '/build/',
        filename: 'index.js'
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader?presets[]=es2015&presets[]=react'
            }, {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract('style-loader', 'css-loader')
            }
        ]
    },
    plugins: [
        // 配置提取出的样式文件
        new ExtractTextPlugin('index.css')
    ],
    //设置代理，解决本地服务器提交ajax请求跨域问题
  devServer: {
    // proxy: {
    //   '/bak/statistics_data': {
    //     target: 'http://dev.ms.tms.codoon.com',
    //     secure: false,
    //     changeOrigin: true
    //   }
    // }
  }
}
//生产环境下
var prod = process.env.NODE_ENV === 'production';
module.exports.plugins = (module.exports.plugins || []);
if (prod) {
    module.exports.plugins = module.exports.plugins.concat([
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': '"production"'
        }),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            },
            comments: false//压缩文件中不包括任何注释
        }),
        new webpack.optimize.OccurenceOrderPlugin()
    ]);
} 
