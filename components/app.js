import React, { Component } from 'react';
import Dropdown from '../sub-components/Dropdown';
import Menus from '../sub-components/Menus';
import $ from '../utils/ajax';
import APIs from '../utils/APIs';

export default class app extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isShow: true,
            userInfo: {}
        }
    }
    componentWillMount() {
        // 进行ajax请求，获取个人信息
        this.getUserInfo();
    }
    getUserInfo() {
        let that = this;
        $.get(APIs.login.url, function (response) {
            let state = response.status.state;
            if (state === 0) {
                that.setState({
                    userInfo: response.data
                });
            } else {
                window.location.href = response.data.rdurl;
            }
        }, function () {
            //console.log('请求中...');
        });
    }
    /**
     * 是否显示左侧导航菜单
     */
    handleLeftBar() {
        this.setState({
            isShow: !this.state.isShow
        });
    }

    render() {
        let isShow = this.state.isShow,
            userInfo=this.state.userInfo,
            leftBar = isShow
                ? ''
                : ' hide',
            content = isShow
                ? ''
                : ' show_all';
        //通过获取个人信息
        const dropMenus = [
            {
                menu_icon: 'icon-shezhi',
                menu_name: '设置',
                menu_url: '/develop'
            }, {
                menu_icon: 'icon-tongzhi-copy',
                menu_name: '通知',
                menu_url: '/develop'
            }, {
                menu_icon: 'icon-iconfontboxno',
                menu_name: '帮助',
                menu_url: '/develop'
            }, {
                menu_icon: 'icon-yaochinc',
                menu_name: '退出',
                menu_url: '/'
            }
        ];
        //定义左侧导航菜单
        const menus = [
            {
                menu_icon: 'icon-dashboard',
                menu_name: '总览',
                menu_url: '/',
                sub_menus: []
            }, {
                menu_icon: 'icon-zhibiaoyuce',
                menu_name: '性能指标',
                menu_url: '/target',
                sub_menus: [
                    {
                        sub_name: '白屏时间',
                        sub_url: '/target/whitescreen'
                    }, {
                        sub_name: '标准首屏时间',
                        sub_url: '/target/standardfs'
                    },{
                        sub_name: '非标准首屏时间',
                        sub_url: '/target/firstscreen'
                    },  {
                        sub_name: '请求耗时',
                        sub_url: '/target/requesttime'
                    },{
                        sub_name: '客户端耗时',
                        sub_url: '/target/clienttime'
                    },
                ]
            }
        ];
        return (
            <div>
                <div className="top_nav_bar">
                    <div
                        className="catalog"
                        onClick={this
                            .handleLeftBar
                            .bind(this)}>
                        <i className="iconfont icon-fenlei"></i>
                    </div>
                    <a href="#" className="web_title">PERFOR<span className="title_end">MANCE</span>
                    </a>
                    <div className="drop_down"><Dropdown self={userInfo} menus={dropMenus} /></div>
                </div>
                <div className={'left_nav_bar' + leftBar}><Menus menus={menus} /></div>
                <div className={'content_container' + content}>
                    {this.props.children}
                </div>
            </div>
        )
    }
}
