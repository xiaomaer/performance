
/* Created by xiaoma on 2017/3/3.
*/
import React, { Component } from 'react';
import $ from '../utils/ajax';
import APIs from '../utils/APIs';
import TheadIndex from '../sub-components/TheadIndex';
import TbodyIndex from '../sub-components/TbodyIndex';
import Page from '../sub-components/Page';
import SimpleLineChart from '../sub-components/LineChart';
import VerticalBar from '../sub-components/BarChart';

export default class Detail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {},//请求返回数据
            page_size: 10,//每页显示多少条记录
            page_num: 1,
            name: "",//页面名称
            url: "",//页面URL
            pm_c: "",//站点码.页面码
            start_date: "",//开始时间
            end_date: "",//结束时间
            tag: 0,//默认是按照访问时间先后顺序排序。0:访问时间，1:客户端耗时，2:请求耗时，3：白屏时间，4：标准首屏时间，5:非标准首屏时间，6:页面URL
            order: true,//true:降序，false:升序
            lineData: [],//绘制linechart所需数据
            chartTitle: '',//绘制chart图表tittle
            dname: "",//暂存右侧绘制的页面名称
            durl: "",//暂存右侧绘制的页面URL
            dpm_c: ""//暂存右侧绘制的站点码.页面码
        }
        this.bindFunctions();
    }
    componentDidMount() {
        // 页面初始化渲染
        this.getData("search",(data) => {
            //初始化渲染表格
            this.setState({
                data: data
            });
            //获取第一条记录的全部数据进行绘图
            const item = data.list[0];
            this.setState({
                dname: item.name,
                durl: item.url,
                dpm_c: item.pm_c,
                chartTitle: item.name || item.url || item.pm_c
            }, () => {
                /*绘制当天的数据哦
                let { data, lineData } = this.state,
                    item = data.list[index],
                    temp = [];
                temp.push({
                    name: item.report_date, "白屏时间": item.whiteScreenTime
                });
                this.setState({
                    lineData: temp
                });*/
                this.handleChartData();
            })
        });
    }
    //函数绑定作用域
    bindFunctions() {
        this.viewChart = this.viewChart.bind(this);
        this.handlePage = this.handlePage.bind(this);
        this.prevPage = this.prevPage.bind(this);
        this.nextPage = this.nextPage.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleDateSearch = this.handleDateSearch.bind(this);
    }
    //根据条件获取渲染数据
    getData(type,callback) {
        let that = this,
            page_size = that.state.page_size,
            page_num = that.state.page_num,
            name = (type === "search") ? that.state.name : that.state.dname,
            url = (type === "search") ? that.state.url : that.state.durl,
            pm_c = (type === "search") ? that.state.pm_c : that.state.dpm_c,
            start_date = (type === "search") ?"":that.state.start_date,
            end_date = (type === "search") ?"":that.state.end_date,
            tag = that.state.tag,
            order = that.state.order,
            params = '?page_size=' + page_size + '&page_num=' + page_num + '&name=' + name +
                '&url=' + url + '&start_date=' + start_date + '&end_date=' + end_date +
                '&tag=' + tag + '&order=' + order + '&pm_c=' + pm_c,
            api = APIs.performanceData.url + params;
        $.get(api, function (response) {
            let state = response.status.state,
                data = response.data;
            if (state === 0) {
                if (callback) {
                    callback(data);
                    return;
                }
                that.setState({
                    data: data
                })

            } else {
                if (state === 5) {
                    setTimeout(function () {
                        window.location.href = response.data.rdurl;
                    }, 300);
                } else {
                    console.log(response.status.msg);
                }
            }
        }, function (error) {
            //console.log('请求中...');
        });
    }
    //根据页面名称／URL/站点码查询
    handleSearch(e) {
        e.preventDefault();
        this.setState({
            page_num: 1
        }, () => {
            this.getData("search");
        })
    }
    //根据选择时间段重绘图表
    handleDateSearch(e) {
        e.preventDefault();
        const { start_date, end_date } = this.state;
        if (start_date > end_date) {
            alert('开始时间不能大于结束时间')
            return;
        }
        this.handleChartData();
    }
    //按照图表数据格式，对数据进行处理
    handleChartData() {
        this.setState({
            page_num: 1
        }, () => {
            this.getData("draw",(data) => {
                const chartData = data.list.reverse().map(function (item, index) {
                    let obj = {};
                    obj.name = item.report_date;
                    obj['首屏时间'] = item.unbelievableTime;
                    return obj;
                });
                this.setState({
                    lineData: chartData
                });
            });
        })
    }
    //获取input输入框改变后的值
    handleInputChange(e) {
        const target = e.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        })
    }
    //查看图表
    viewChart(index, name, url, pm_c) {
        this.setState({
            dname: name,
            durl: url,
            dpm_c: pm_c,
            start_date: "",
            end_date: "",
            chartTitle: name || url || pm_c
        }, () => {
            /*let { data, lineData } = this.state,
                item = data.list[index],
                temp = [];
            temp.push({
                name: item.report_date, "白屏时间": item.whiteScreenTime
            });
            this.setState({
                lineData: temp
            });*/
            this.handleChartData();
        })
    }
    //点击页码进行切换
    handlePage(e) {
        let currPage = parseInt(e.target.dataset['id']);
        this.setState({
            page_num: currPage
        }, () => {
            //进行查询请求
            this.getData("search");
        });
    }
    //上一页
    prevPage(e) {
        e.stopPropagation();
        let currPage = this.state.page_num;
        if (currPage === 1) return;
        this.setState({
            page_num: currPage - 1
        }, () => {
            //进行查询请求
            this.getData("search");
        });
    }
    //下一页
    nextPage(e) {
        e.stopPropagation();
        let currPage = this.state.page_num;
        if (currPage >= this.state.data.page_total) return;
        this.setState({
            page_num: currPage + 1
        }, () => {
            //进行查询请求
            this.getData("search");
        });
    }
    render() {
        const bardata = [
            { name: '首屏时间评估', "很快(0-0.5s)": 0.5, "较快(0.5s-1s)": 1, "用户可接受(1s-2s)": 2, "很慢(2s+)": 4 }
        ],
            barconfig = [
                { name: "很快(0-0.5s)", color: "#a9d96c" },
                { name: "较快(0.5s-1s)", color: "#6ccac9" },
                { name: "用户可接受(1s-2s)", color: "#f8d347" },
                { name: "很慢(2s+)", color: "#ff6c60" }
            ]
        let { data, chartTitle, lineData,page_num, page_size} = this.state;
        return (
            <div className="page_content">
                <div className="parts">
                    <div className="content_area section_one">
                        <div className="table_name">
                            <span>非标准首屏时间</span>
                            <div className="tip">
                                <i className="iconfont icon-questioncircle"></i>
                                <div className="info_tip">
                                    非标准首屏时间：页面所有资源加载完成的时间<br />[主要影响因素：样式、脚本、媒体文件等下载时间]
                            </div>
                            </div>
                        </div>
                        <div className="table_area">
                            <form className="form_area" onSubmit={this.handleSearch}>
                                <input type="text" placeholder="请输入页面名称" className="base-unit" name="name" value={this.state.name} onChange={this.handleInputChange} />
                                <input type="text" placeholder="请输入页面URL" className="base-unit" name="url" value={this.state.url} onChange={this.handleInputChange} />
                                <input type="text" placeholder="请输入站点码.页面码" className="base-unit" name="pm_c" value={this.state.pm_c} onChange={this.handleInputChange} />
                                <input type="submit" value="搜索" className="base-unit" />
                            </form>
                            <table className="table">
                                <TheadIndex index="非标准首屏时间" />
                                <TbodyIndex list={data.list} viewChart={this.viewChart} indexName="unbelievableTime" pageNum={page_num} pageSize={page_size}/>
                            </table>
                            {!data.list? (<div className="no_results">加载中...</div>) : ''}
                            {data.list && data.list.length === 0 ? (<div className="no_results">没有符合条件的信息！</div>) : ''}
                            <div className="pages">
                                <Page page={data} handlePage={this.handlePage} prevPage={this.prevPage} nextPage={this.nextPage} />
                            </div>
                        </div>
                    </div>
                    <div className="content_area section_two">
                        <div className="table_name">当前页面为：{chartTitle}</div>
                        <div className="table_area">
                            <form className="form_area" onSubmit={this.handleDateSearch}>
                                <label>开始时间：</label>
                                <input type="date" placeholder="请选择开始时间" className="base-unit" name="start_date" value={this.state.start_date} onChange={this.handleInputChange} />
                                <label>结束时间：</label>
                                <input type="date" placeholder="请选择结束时间" className="base-unit" name="end_date" value={this.state.end_date} onChange={this.handleInputChange} />
                                <input type="submit" value="重绘" className="base-unit" />
                            </form>
                        </div>
                        <div className="table">
                            <SimpleLineChart lineData={lineData} label="首屏时间（单位：ms）" index="首屏时间" />
                            <VerticalBar data={bardata} config={barconfig} />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}