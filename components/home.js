/**
 * Created by xiaoma on 2017/3/3.
 */
import React, { Component } from 'react';
import Tbody from '../sub-components/Tbody';
import Page from '../sub-components/Page';
import ChartDialog from '../sub-components/ChartDialog';
import $ from '../utils/ajax';
import APIs from '../utils/APIs';

export default class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            page_size: 20,//每页显示多少条记录
            page_num: 1,
            name: "",//页面名称
            url: "",//页面URL
            pm_c: "",//站点码.页面码
            start_date: "",//开始时间
            end_date: "",//结束时间
            tag: 0,//默认是按照访问时间先后顺序排序。0:访问时间，1:客户端耗时，2:请求耗时，3：白屏时间，4：标准首屏时间，5:非标准首屏时间，6:页面URL
            order: true,//true:降序，false:升序
            data: {},//请求返回数据
            index_state: ["", 0, 0, 0, 0, 0, 0],//控制指标升降序箭头，0:默认，1:降序，2:升序
            isShowChartDialog: false,
            chartData: [],//绘制areachart所需数据
            chartTitle: '',//绘制areachart图表tittle
            selectedValue: '0'
        }
    }
    componentDidMount() {
        // 进行ajax请求
        this.getData();//获取页面性能指标
    }
    //根据条件获取渲染数据
    getData(callback) {
        let that = this,
            page_size = that.state.page_size,
            page_num = that.state.page_num,
            name = that.state.name,
            url = that.state.url,
            pm_c = that.state.pm_c,
            start_date = that.state.start_date,
            end_date = that.state.end_date,
            tag = that.state.tag,
            order = that.state.order,
            params = '?page_size=' + page_size + '&page_num=' + page_num + '&name=' + name +
                '&url=' + url + '&start_date=' + start_date + '&end_date=' + end_date +
                '&tag=' + tag + '&order=' + order + '&pm_c=' + pm_c,
            api = APIs.performanceData.url + params;
        $.get(api, function (response) {
            let state = response.status.state,
                data = response.data;
            if (state === 0) {
                if (callback) {
                    callback(data);
                    return;
                }
                that.setState({
                    data: data
                })

            } else {
                if (state === 5) {
                    setTimeout(function () {
                        window.location.href = response.data.rdurl;
                    }, 300);
                } else {
                    console.log(response.status.msg);
                }
            }
        }, function (error) {
            //console.log('请求中...');
        });
    }
    handleSearch(e) {
        e.preventDefault();
        this.setState({
            page_num: 1
        }, () => {
            this.getData();
        })
    }
    //获取input输入框改变后的值
    handleInputChange(e) {
        const target = e.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        })
        if (name === 'start_date') {
            this.setState({
                end_date: value
            })
        }
    }
    //点击页码进行切换
    handlePage(e) {
        let currPage = parseInt(e.target.dataset['id']);
        this.setState({
            page_num: currPage
        }, () => {
            //进行查询请求
            this.getData();
        });
    }
    //上一页
    prevPage(e) {
        e.stopPropagation();
        let currPage = this.state.page_num;
        if (currPage === 1) return;
        this.setState({
            page_num: currPage - 1
        }, () => {
            //进行查询请求
            this.getData();
        });
    }
    //下一页
    nextPage(e) {
        e.stopPropagation();
        let currPage = this.state.page_num;
        if (currPage >= this.state.data.page_total) return;
        this.setState({
            page_num: currPage + 1
        }, () => {
            //进行查询请求
            this.getData();
        });
    }
    //点击指标进行排序
    handleOrder(e) {
        let target = e.target,
            elem = (target.tagName.toLowerCase() === 'th') ? target : target.parentNode,
            tag = elem.dataset['tag'] ? parseInt(elem.dataset['tag']) : undefined;
        if (tag === undefined) return;
        let order = this.state.order,
            index_state = this.state.index_state;
        if (tag === 0 || tag == 6) {
            order = false;
        } else {
            if (this.state.tag !== tag) {
                order = false;
            }
        }

        for (let i = 0, len = this.state.index_state.length; i < len; i++) {
            if (i === tag) {
                index_state[i] = order ? 2 : 1;
            } else {
                index_state[i] = 0;
            }
        }


        this.setState({
            page_num: 1,
            tag: tag,
            order: !order,
            index_state: index_state
        }, () => {
            this.getData();
        })

    }
    //打开绘图弹窗
    showChartDialog(name, url, pm_c) {
        let start_time = this.getDate(7),//对话框打开时，默认获取7天的数据绘制图表
            end_time = this.getDate(0);//获取当前日期
        this.setState({
            name: name,
            url: url,
            pm_c: pm_c,
            chartTitle: name || url || pm_c,
            start_date: start_time,
            end_date: end_time,
            page_num: 1
        }, () => {
            //处理绘制图表的数据
            this.handleChartData();
        });
    }
    //关闭绘图弹窗
    closeChartDialog() {
        this.setState({
            name: "",
            url: "",
            pm_c: "",
            chartTitle: "",
            start_date: "",
            end_date: "",
            isShowChartDialog: false,
            selectedValue: "0"
        })
    }
    //根据时间段重新获取绘制的数据
    changeChartData(duration) {
        let diffDay = 0;
        switch (duration) {
            case "0":
                diffDay = 7;
                break;
            case "1":
                diffDay = 15;
                break;
            case "2":
                diffDay = 21;
                break;
            case "3":
                diffDay = 30;
                break;
        }
        let start_time = this.getDate(diffDay),//获取多少天之前的日期
            end_time = this.getDate(0);//获取当前日期
        this.setState({
            start_date: start_time,
            end_date: end_time,
            selectedValue: duration
        }, () => {
            //处理绘制图表的数据
            this.handleChartData();
        });
    }
    //处理绘制图表的数据
    handleChartData() {
        const that = this;
        this.getData(function (data) {
            const chartData = data.list.reverse().map(function (item, index) {
                let obj = {};
                obj.name = item.report_date;
                obj['客户端耗时'] = item.clientRequestTime;
                obj['请求耗时'] = item.requestTime;
                obj['白屏时间'] = item.whiteScreenTime;
                obj['标准首屏时间'] = item.credibleTime;
                obj['非标准首屏时间'] = item.unbelievableTime;
                return obj;
            });
            that.setState({
                chartData: chartData,
                isShowChartDialog: true
            });
        });
    }
    //获取当前日期前后n天日期
    getDate(diffDay) {
        let date = new Date();
        date.setDate(date.getDate() - diffDay);
        let year = date.getFullYear(),
            month = (date.getMonth() + 1) < 10 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1),
            day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
        return year + "-" + month + "-" + day;
    }
    render() {
        const { data, index_state, chartData, isShowChartDialog, chartTitle, page_num, page_size, selectedValue } = this.state;
        const config = [
            { title: '客户端耗时', strokeColor: '#6ccac9', fillColor: '#6ccac9' },
            { title: '请求耗时', strokeColor: '#ff6c60', fillColor: '#ff6c60' },
            { title: '白屏时间', strokeColor: '#f8d347', fillColor: '#f8d347' },
            { title: '标准首屏时间', strokeColor: '#57c8f2', fillColor: '#57c8f2' },
            { title: '非标准首屏时间', strokeColor: '#a9d96c', fillColor: '#a9d96c' }
        ];
        return (
            <div className="page_content">
                <ChartDialog title={chartTitle} data={chartData} config={config} onClose={this.closeChartDialog.bind(this)} onChange={this.changeChartData.bind(this)} isShow={isShowChartDialog} selectedValue={selectedValue} />
                <div className="content_area">
                    <div className="table_name">
                        <span>总览页面性能指标</span>
                        <div className="tip">
                            <i className="iconfont icon-questioncircle"></i>
                            <div className="info_tip">
                                页面加载时间是根据js探针获取一个网页加载过程中的Navigation start，First byte，DOM ready，Onload的时间计算的结果。<br />
                                客户端耗时：请求等候服务器处理消耗的时间。<br />
                                请求耗时：tcp连接到请求响应结束所需时间。<br />
                                白屏时间：加载网页到DOM模型建立消耗的时间。<br />
                                标准首屏时间：首屏指定位置之前资源加载完成所需时间。<br />
                                非标准首屏时间：首屏所有资源加载完成所需时间。<br />
                            </div>
                        </div>
                    </div>
                    <div className="table_area">
                        <form className="form_area" onSubmit={this.handleSearch.bind(this)}>
                            <input type="text" placeholder="请输入页面名称" className="base-unit" name="name" onChange={this.handleInputChange.bind(this)} />
                            <input type="text" placeholder="请输入页面URL" className="base-unit" name="url" onChange={this.handleInputChange.bind(this)} />
                            <input type="text" placeholder="请输入站点码.页面码" className="base-unit" name="pm_c" onChange={this.handleInputChange.bind(this)} />
                            <input type="date" placeholder="请选择时间" className="base-unit" name="start_date" onChange={this.handleInputChange.bind(this)} />
                            <input type="submit" value="搜索" className="base-unit" />
                        </form>
                        <table className="table">
                            <thead>
                                <tr onClick={this.handleOrder.bind(this)}>
                                    <th className="td_5">序号</th>
                                    <th data-tag='0'>访问时间</th>
                                    <th>页面名称</th>
                                    <th className="td_16" data-tag='6'>
                                        {/*<span>页面URL</span>
                                        <i className={'iconfont' + (index_state[6] ? (index_state[6] == 1 ? ' icon-sort-small-copy-copy1' : ' icon-sort-small-copy-copy') : ' icon-sort-down-copy')}></i>*/}
                                        页面URL
                                    </th>
                                    <th className="td_16">站点码.页面码</th>
                                    <th data-tag='1'><span>客户端耗时</span><i className={'iconfont' + (index_state[1] ? (index_state[1] == 1 ? ' icon-sort-small-copy-copy1' : ' icon-sort-small-copy-copy') : ' icon-sort-down-copy')}></i></th>
                                    <th data-tag='2'><span>请求耗时</span><i className={'iconfont' + (index_state[2] ? (index_state[2] == 1 ? ' icon-sort-small-copy-copy1' : ' icon-sort-small-copy-copy') : ' icon-sort-down-copy')}></i></th>
                                    <th data-tag='3'><span>白屏时间</span><i className={'iconfont' + (index_state[3] ? (index_state[3] == 1 ? ' icon-sort-small-copy-copy1' : ' icon-sort-small-copy-copy') : ' icon-sort-down-copy')}></i></th>
                                    <th data-tag='5'><span>非标准首屏时间</span><i className={'iconfont' + (index_state[5] ? (index_state[5] == 1 ? ' icon-sort-small-copy-copy1' : ' icon-sort-small-copy-copy') : ' icon-sort-down-copy')}></i></th>
                                    <th data-tag='4'><span>标准首屏时间</span><i className={'iconfont' + (index_state[4] ? (index_state[4] == 1 ? ' icon-sort-small-copy-copy1' : ' icon-sort-small-copy-copy') : ' icon-sort-down-copy')}></i></th>
                                    <th>错误次数占比</th>
                                    <th className="td_7">查看图表</th>
                                </tr>
                            </thead>
                            <Tbody list={data.list} showDialog={this.showChartDialog.bind(this)} pageNum={page_num} pageSize={page_size} />
                        </table>
                        {!data.list? (<div className="no_results">加载中...</div>) : ''}
                        {data.list && data.list.length === 0 ? (<div className="no_results">没有符合条件的信息！</div>) : ''}
                        <div className="pages">
                            {/*<span>共123条，当前展示1-10条</span>*/}
                            <Page page={data} handlePage={this.handlePage.bind(this)} prevPage={this.prevPage.bind(this)} nextPage={this.nextPage.bind(this)} />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
