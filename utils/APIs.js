/*
*接口环境配置
**/
let hostname = window.location.hostname,
    actualHost=(hostname==='localhost')?'dev.ms.tms.codoon.com':hostname,
    APIs = {
        'performanceData': {
            url: "//" + actualHost + "/bak/statistics_data",
            methods: ['GET'],
            required: [{
                'key': 'page_size',
                'type': 'number',
                'desc': '每页条数'
            }, {
                'key': 'page_num',
                'type': 'number',
                'desc': '第几页'
            }, {
                'key': 'name',
                'type': 'string',
                'desc': '页面名称'
            }, {
                'key': 'url',
                'type': 'string',
                'desc': '页面URL'
            }, {
                'key': 'report_date',
                'type': 'string',
                'desc': '监控时间'
            }],
            desc: "获取页面性能列表",
        },
        'login': {
            url: '//' + actualHost  + '/bak/permission',
            methods: ['get'],
            desc: '用户.权限获取'
        },
        'loginout': {
            url: '//' + actualHost  + '/bak/logout',
            methods: ['post'],
            desc: '用户.登出'
        }
    };
window.LOGINPAGE=actualHost;
export default APIs;
