let ajax = {
    get: function (url, success, error) {
        let xhr = new XMLHttpRequest();
        xhr.open('get', url, true);
        xhr.withCredentials = true;
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                let response = JSON.parse(xhr.responseText);
                success & success(response);
            } else {
                error && error();
            }
        };
        xhr.send();
    },
    post: function (url, params, success, error) {
        let xhr = new XMLHttpRequest();
        xhr.open('post', url, true);
        xhr.withCredentials = true;
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                let response = JSON.parse(xhr.responseText);
                success & success(response);
            } else {
                error && error();
            }
        };
        xhr.send(params);
    }
}
export default ajax;
