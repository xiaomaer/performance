/**
 * Created by xiaoma on 2017/3/24.
 */
import React, { Component } from 'react';
import { Link } from 'react-router';
import $ from '../utils/ajax';
import APIs from '../utils/APIs';

export default class Dropdown extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isHide: true
        }
    }
    showMenus() {
        this.setState({
            isHide: !this.state.isHide
        })
    }
    loginOut() {
        let that = this;
        $.post(APIs.loginout.url, '', function (response) {
            let state = response.status.state;
            if (state === 0) {
                window.location.href = '//'+LOGINPAGE+':3300/index.html?ref='+encodeURIComponent(window.location.href);
            } else {
                console.log('退出失败');
            }
        }, function () {
            console.log('请求失败');
        });
    }
    render() {
        let { self, menus } = this.props,
            menusLen = menus.length - 1,
            isHide = this.state.isHide,
            showClass = isHide ? '' : 'show';
        const dropMenus = menus.map((item, index) => {
            const isFinal = (index === menusLen) ? ' log_out' : '';
            return (
                <li className={'drop_down_menu' + isFinal} key={'item_' + index} onClick={isFinal ? this.loginOut.bind(this) : ''}>
                    <Link to={item.menu_url}>
                        <i className={'iconfont ' + item.menu_icon}></i>
                        <div>{item.menu_name}</div>
                    </Link>
                </li>
            )
        });
        return (
            <div>
                <div className="drop_down_toggle" onClick={this.showMenus.bind(this)}>
                    <img src={self.avatar || '//activity-codoon.b0.upaiyun.com/cdmall62762451691725..name'} className="user_image" />
                    <span className="user_name">{self.nick}</span>
                    <b className="triangle"></b>
                </div>
                <ul className={'drop_down_menus ' + showClass}>{dropMenus}</ul>
            </div>
        );
    }
}
