import React, { Component } from 'react';

export default class Tbody extends Component {
    constructor(props) {
        super(props);
        this.showChart = this.showChart.bind(this);
    }
    showChart(event) {
        let index = parseInt(event.target.dataset['index']),
            pageData = this.props.list[index],
            name = pageData.name,
            url = pageData.url,
            pm_c = pageData.pm_c;
        this.props.showDialog(name, url, pm_c);
    }
    render() {
        let { list, pageNum, pageSize } = this.props,
            lines = [],
            that = this;
        if (list) {
            lines = list.map(function (item, index) {
                return (
                    <tr data-id={item.id} key={'line_' + index}>
                        <td>{(pageNum - 1) * pageSize + (index + 1)}</td>
                        <td>{item.report_date}</td>
                        <td>{item.name}</td>
                        <td>{item.url}</td>
                        <td>{item.pm_c}</td>
                        <td>{item.clientRequestTime + 'ms'}</td>
                        <td>{item.requestTime + 'ms'}</td>
                        <td className={item.whiteScreenTime > 500 ? 'overindex' : ''}>{item.whiteScreenTime + 'ms'}</td>
                        <td className={item.unbelievableTime > 2000 ? 'overindex' : ''}>{(item.unbelievableTime === 0) ? '-' : (item.unbelievableTime + 'ms')}</td>
                        <td className={item.credibleTime > 2000 ? 'overindex' : ''}>{(item.credibleTime === 0) ? '-' : (item.credibleTime + 'ms')}</td>
                        <td className={item.has_err > 0.3 ? 'overindex' : ''}>{item.has_err}</td>
                        <td>
                            {/*<button type="button" className="btn_view" data-index={index} onClick={that.showChart}>对比</button>*/}
                            <i className="iconfont icon-tubiao" data-index={index} onClick={that.showChart}></i>
                        </td>
                    </tr>
                )
            });
        }
        return (
            <tbody>{lines}</tbody>
        )
    }
}