//单个性能指标的表内容
import React, { Component } from 'react';
export default class TbodyIndex extends Component {
    constructor(props) {
        super(props);
        this.showChart = this.showChart.bind(this);
    }
    showChart(event) {
        let index = parseInt(event.target.dataset['index']),
            pageData = this.props.list[index],
            name = pageData.name,
            url = pageData.url,
            pm_c = pageData.pm_c;
        this.props.viewChart(index, name, url, pm_c);
    }
    render() {
        const { list, indexName,pageNum, pageSize} = this.props,
            that = this;
        let lines = [];
        if (list) {
            lines = list.map(function (item, index) {
                return (
                    <tr data-id={item.id} key={'line_' + index}>
                        <td>{(pageNum - 1) * pageSize + (index + 1)}</td>
                        <td>{item.report_date}</td>
                        <td>{item.name}</td>
                        <td>{item.url}</td>
                        <td>{item.pm_c}</td>
                        <td>{(indexName === 'credibleTime' && item[indexName] === 0) ? '-' : (item[indexName] + 'ms')}</td>
                        <td>
                            {/*<button type="button" className="btn_view" data-index={index} onClick={that.showChart}>对比</button>*/}
                            <i className="iconfont icon-tubiao" data-index={index} onClick={that.showChart}></i>
                        </td>
                    </tr>
                )
            });
        }
        return (
            <tbody>{lines}</tbody>
        )
    }
}