import React, { Component } from 'react';
import { AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'Recharts';

export default class AreaLine extends Component {
    render() {
        const { data, config } = this.props;
        const areas = config.map(function (item, index) {
            return (<Area type='monotone' dataKey={item.title} stackId="1" stroke={item.strokeColor} fill={item.fillColor} key={index} />)
        });
        return (
            <AreaChart width={600} height={350} data={data} margin={{ top: 10, right: 30, left: 0, bottom: 0 }}>
                <XAxis dataKey="name"/>
                <YAxis label={{ value: "单位：ms", position: "insideLeft", angle: -90, dy: 30 }} tickCount={7}/>
                <CartesianGrid strokeDasharray="3 3" />
                <Tooltip />
                <Legend />
                {areas}
            </AreaChart>
        )
    }
}