import React, { Component } from 'react';
import AreaChart from './AreaChart';

export default class ChartDialog extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        let duration = event.target.value;
        this.props.onChange(duration);
    }
    render() {
        const { title, data, config, onClose, isShow, selectedValue } = this.props;
        // let style = isShow ? { display: "block" } : { display: "none" };
        const mask_show = isShow ? ' chart_mask_show' : '';
        const chart_show = isShow ? ' chart_show' : '';
        return (
            <div>
                <div className={"chart_mask" + mask_show}></div>
                <div className={"chart" + chart_show}>
                    <div className="chart_title">
                        <button type="button" className="btn_close" onClick={onClose}>×</button>
                        <h1>页面性能指标-{title}</h1>
                    </div>
                    <div className="time-picker">
                        {/*<div className="data_unit">单位：ms</div>*/}
                        <label>请选择页面性能比较时间段：</label>
                        <select className="base-unit select-time" value={selectedValue} onChange={this.handleChange}>
                            <option value="0">一周</option>
                            <option value="1">半个月</option>
                            <option value="2">三周</option>
                            <option value="3">一个月</option>
                        </select>
                    </div>
                    <div className="chart_type">
                        <AreaChart data={data} config={config} />
                    </div>
                </div>
            </div>
        )
    }

}