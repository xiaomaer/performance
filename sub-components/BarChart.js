/**
 * Created by xiaoma on 2017/3/3.
 */
import React, { Component } from 'react';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'Recharts';

export default class VerticalBar extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const { data, config } = this.props;
        const bars = config.map((item, index) => {
            return (<Bar dataKey={item.name} stackId="a" fill={item.color} key={index} />)
        });
        return (
            <BarChart layout="vertical" barSize={50} width={500} height={240} data={data}
                margin={{ top: 0, right: 0, left: 30, bottom: 20 }}>
                <XAxis type="number" />
                <YAxis dataKey="name" type="category" />
                <CartesianGrid strokeDasharray="3 3" label />
                {/*<Tooltip />*/}
                <Legend />
                {bars}
            </BarChart>
        )
    }
}
