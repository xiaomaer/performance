import React, { Component } from 'react';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'Recharts';

export default class SimpleLineChart extends Component {
    render() {
        const { lineData, label, index } = this.props;
        return (
            <LineChart width={500} height={340} data={lineData}
                margin={{ top: 0, right: 10, left: 0, bottom: 70 }}>
                <XAxis dataKey="name" interval={0} tick={{ angle: -60, dx: -20, dy: 35 }} />
                <YAxis label={{ value: label, position: "insideLeft", angle: -90, dx: 5, dy: 80 }} tickCount={6}/>
                <CartesianGrid strokeDasharray="3 3" />
                <Tooltip />
                <Legend verticalAlign="top"  height={30}/>
                <Line type="monotone" dataKey={index} stroke="#78CD51" activeDot={{ r: 8 }} />
            </LineChart>
        )
    }
}