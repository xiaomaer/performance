import React, { Component } from 'react';
import { Link } from 'react-router';

export default class SubMenus extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        let {subMenus,index,showMenuItem} = this.props,
            isShow=(showMenuItem === index) ? ' show' : '';
        const subMenuItems = subMenus.map((item, index) => {
            return (
                <li className="sub-menu" key={'sub_' + index}>
                    <Link to={item.sub_url} activeClassName="sub-active">{item.sub_name}</Link>
                </li >
            )
        });
        return (
            <ul className={'sub-menus'+isShow}>{subMenuItems}</ul>
        )
    }
}