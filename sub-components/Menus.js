/**
 * Created by xiaoma on 2017/3/24.
 */
import React, {Component} from 'react';
import {Link, IndexLink} from 'react-router';
import SubMenus from './SubMenus';

export default class Menus extends Component {
    constructor(props) {
        super(props);
        this.state = {
            menuIndex: -1,
            curMenu: -1
        };
    }
    toggleMenu(index) {
        let currIndex = this.state.menuIndex;
        this.setState({
            curMenu:index
        });
        //菜单进行切换时第一次点击不显示二级菜单，要点击第二次才显示
        if(currIndex!==-1&&currIndex!==index){
             currIndex =-1;
        }
        if (currIndex === -1) {
            this.setState({menuIndex: index});
        } else {
            this.setState({menuIndex: -1});
        }

    }
    hideSubMenu(){
        //没有二级菜单点击时，直接不显示即可
        this.setState({menuIndex: -1});
    }
    render() {
        let menus = this.props.menus;
        const menuItems = menus.map((item, index) => {
            let subMenus = item.sub_menus;
            if (subMenus.length === 0) {
                return (
                    <li
                        className="menu"
                        key={'menu_' + index}
                        onClick={this
                        .hideSubMenu
                        .bind(this)}>
                        <IndexLink to={item.menu_url} activeClassName="active">
                            <i className={'iconfont ' + item.menu_icon + ' menu-icon'}></i>
                            <span>{item.menu_name}</span>
                        </IndexLink>
                    </li>
                )
            } else {
                let extend_icon=' icon-jiahao ';
                if(this.state.curMenu===index){
                    extend_icon = (this.state.menuIndex === -1)
                    ? ' icon-jiahao '
                    : ' icon-jianhao ';
                }
                return (
                    <li className="menu" key={'menu' + index}>
                        <Link
                            to={item.menu_url}
                            activeClassName="active"
                            onClick={this.toggleMenu.bind(this, index)}>
                            <i className={'iconfont ' + item.menu_icon + ' menu-icon'}></i>
                            <span>{item.menu_name}</span>
                            <i className={'iconfont' + extend_icon + 'extend-icon'}></i>
                        </Link>
                        <SubMenus
                            subMenus={subMenus}
                            index={index}
                            showMenuItem={this.state.menuIndex}/>
                    </li>
                )
            }
        });
        return (
            <ul role="nav" className="menus">{menuItems}</ul>
        )
    }
}
