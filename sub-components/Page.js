import React, { Component } from 'react';

export default class Page extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        let { page, handlePage, prevPage, nextPage } = this.props,
            page_total = page.page_total,
            page_num = page.page_num,
            item = [];
        if (page_total <= 5) {
            for (let i = 1; i <= page_total; i++) {
                if (i === page_num) {
                    item.push(<li className="active" key={'page_' + i}><span data-id={i}>{i}</span></li>);
                } else {
                    item.push(<li key={'page_' + i}><span data-id={i}>{i}</span></li>);
                }
            }
        } else {
            if (page_num < 5) {
                for (let i = 1; i <= 5; i++) {
                    if (i === page_num) {
                        item.push(<li className="active" key={'page_' + i}><span data-id={i}>{i}</span></li>);
                    } else {
                        item.push(<li key={'page_' + i}><span data-id={i}>{i}</span></li>);
                    }
                }
                item.push(<li className="ellipsis" key="last">...</li>);
            } else if (page_num > (page_total - 3)) {
                item.push(<li className="ellipsis" key="first">...</li>);
                for (let i = (page_total - 4); i <= page_total; i++) {
                    if (i === page_num) {
                        item.push(<li className="active" key={'page_' + i}><span data-id={i}>{i}</span></li>);
                    } else {
                        item.push(<li key={'page_' + i}><span data-id={i}>{i}</span></li>);
                    }
                }
            } else {
                item.push(<li className="ellipsis" key="first">...</li>);
                for (let i = (page_num - 2); i <= (page_num + 2); i++) {
                    if (i === page_num) {
                        item.push(<li className="active" key={'page_' + i}><span data-id={i}>{i}</span></li>);
                    } else {
                        item.push(<li key={'page_' + i}><span data-id={i}>{i}</span></li>);
                    }
                }
                item.push(<li className="ellipsis" key="last">...</li>);
            }
        }
        // for (let i = 1; i <= page_total; i++) {
        //     if (i === page_num) {
        //         item.push(<li className="active" key={'page_' + i}><span  data-id={i}>{i}</span></li>);
        //     } else {
        //         item.push(<li key={'page_' + i}><span data-id={i}>{i}</span></li>);
        //     }
        // }
        return (
            <ul onClick={handlePage}>
                <li className={(page_num === 1) ? 'disabled' : ''} onClick={prevPage}>
                    <span>上一页</span>
                </li>
                {item}
                <li className={(page_num >= page_total) ? 'disabled' : ''} onClick={nextPage}>
                    <span>下一页</span>
                </li>
            </ul>
        )
    }
}