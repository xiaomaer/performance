//单个性能指标的表头
import React, { Component } from 'react';

export default class TheadIndex extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const {index} = this.props;
        return (
            <thead>
                <tr>
                    <th className="td_5">序号</th>
                    <th className="td_15">访问时间</th>
                    <th>页面名称</th>
                    <th className="td_25">页面URL</th>
                    <th className="td_15">站点码.页面码</th>
                    <th>{index}</th>
                    <th>查看图表</th>
                </tr>
            </thead>
        )
    }
}