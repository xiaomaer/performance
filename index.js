import React from 'react';
import { render } from 'react-dom';
import { Router, Route, IndexRoute,IndexRedirect, hashHistory } from 'react-router';

import App from './components/app';
import Home from './components/home';
import Target from './components/target';
import Develop from './components/develop';

import Whitescreen from './components/whitescreen';
import Firstscreen from './components/firstscreen';
import Requesttime from './components/requesttime';
import Clienttime from './components/clienttime';
import StandardFS from './components/standardfs';

import './index.css';

render((
    <Router history={hashHistory}>
        <Route path="/" component={App}>
            <IndexRoute component={Home} />
            <Route path="/target" component={Target}>
                <IndexRedirect to="whitescreen" />
                <Route path="whitescreen" component={Whitescreen} />
                <Route path="firstscreen" component={Firstscreen} />
                <Route path="requesttime" component={Requesttime} />
                <Route path="clienttime" component={Clienttime} />
                <Route path="standardfs" component={StandardFS}/>
            </Route>
            <Route path="/develop" component={Develop}></Route>
        </Route>
    </Router>
), document.getElementById('app'));
